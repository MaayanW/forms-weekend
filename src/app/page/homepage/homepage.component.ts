import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ContactService} from '../../services/conatacts.service';
import {User} from '../../models/user.modle';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
@ViewChild('f') f: NgForm;
contacts:  User[] = [];
    i = 0;
  constructor(private contactService: ContactService) { }

user = {
   name: '',
  phone: '',
  relate: '',
};
  ngOnInit() {
  }
onSubmit(form: NgForm) {
    this.user.name = this.f.value.Name;
    this.user.phone = this.f.value.Phone;
    this.user.relate = this.f.value.relate;
    this.contacts.push(this.user);
    console.log(this.contacts);
    this.f.reset();
  }
  onDelete(username: string) {
   for ( this.i < this.contacts.length; this.i++;) {
      if (username === this.contacts[this.i].name) {
          this.contacts.splice(this.i, 1);
      }
   }
  }
}

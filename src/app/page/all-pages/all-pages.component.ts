import { Component, OnInit } from '@angular/core';
import {ContactService} from '../../services/conatacts.service';

@Component({
  selector: 'app-all-pages',
  templateUrl: './all-pages.component.html',
  styleUrls: ['./all-pages.component.css']
})
export class AllPagesComponent implements OnInit {
contacts = [];
  constructor(private contactService: ContactService) {
    this.contacts = this.contactService.contacts;
  }

  ngOnInit() {
    console.log(this.contacts);
  }

}

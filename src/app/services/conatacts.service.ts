import { Injectable } from '@angular/core';
import {User} from '../models/user.modle';


@Injectable({
  providedIn: 'root'
})
export class ContactService {
  contacts: User[] = [];

  constructor() { }

  addUser(user) {
    this.contacts.push(user);
  }
  getUser() {
    return this.contacts;
  }
}

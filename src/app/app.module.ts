import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { PageComponent } from './page/page.component';
import { HomepageComponent } from './page/homepage/homepage.component';
import { AllPagesComponent } from './page/all-pages/all-pages.component';
import { HeaderComponent } from './page/header/header.component';
import {ContactService} from './services/conatacts.service';

@NgModule({
  declarations: [
    AppComponent,
    PageComponent,
    HomepageComponent,
    AllPagesComponent,
    HeaderComponent

  ],
  imports: [
    BrowserModule,
    FormsModule

  ],
  providers: [ContactService],
  bootstrap: [AppComponent]
})
export class AppModule { }
export class AppRoutingModule { }
